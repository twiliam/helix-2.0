package org.firstinspires.ftc.teamcode.apis.core_lib;

import org.firstinspires.ftc.robotcore.internal.camera.libuvc.nativeobject.VuforiaExternalProviderCameraFrame;

import java.net.ConnectException;
import java.io.*;
import java.net.*;

public class HelixLinkService {
    public boolean connected = false;
    Socket clientSocket = null;
    PrintWriter outToServer = null;
    BufferedReader inFromServer = null;

    public void connect(String host, int port) throws IOException {
        clientSocket = new Socket(host, port);
        outToServer = new PrintWriter(clientSocket.getOutputStream());
        inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        connected = clientSocket.isConnected();
    }



    public void issueCommand(String command) {
        outToServer.println(command);
        outToServer.flush();
    }

    public String query(String command) {
        try {
            issueCommand(command);
            return inFromServer.readLine();
        }
        catch (IOException e) {
            return "FAIL";
        }
    }

    public void close() {
        try {
            inFromServer.close();
            outToServer.close();
            clientSocket.close();
        }
        catch (IOException e) {
            System.out.println("Falied to close TCP socket to HELIX LINK");
        }
    }
}
