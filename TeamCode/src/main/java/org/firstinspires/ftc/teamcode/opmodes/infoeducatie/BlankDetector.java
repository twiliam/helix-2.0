package org.firstinspires.ftc.teamcode.opmodes.infoeducatie;

import com.disnodeteam.dogecv.OpenCVPipeline;
import com.disnodeteam.dogecv.detectors.DogeCVDetector;
import com.disnodeteam.dogecv.filters.DogeCVColorFilter;
import com.disnodeteam.dogecv.filters.HSVColorFilter;
import com.disnodeteam.dogecv.filters.LeviColorFilter;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Victo on 12/17/2017.
 */

public class BlankDetector extends DogeCVDetector {
    private GripPipeline pipeline = new GripPipeline();
    private Mat hierarchy = new Mat();

    private MatOfPoint bestContour = null;
    private HSVColorFilter redFilter = new HSVColorFilter(new Scalar(35,224,220),new Scalar(48,90,225));

    private void hsvThreshold(Mat input, double[] hue, double[] sat, double[] val,
                              Mat out) {
        Imgproc.cvtColor(input, out, Imgproc.COLOR_RGB2HSV);
        Core.inRange(out, new Scalar(hue[0], sat[0], val[0]),
                new Scalar(hue[1], sat[1], val[1]), out);
    }

    @Override
    public Mat process(Mat workingMatIn) {
        Mat workingMat = new Mat();
        Mat workingMatOut = new Mat();
        workingMatIn.copyTo(workingMat);

        lastRegisteredWidth = workingMatIn.width();
        lastRegisteredHeight = workingMatIn.height();
        // Process frame
        //redFilter.process(workingMat.clone(), workingMat);
        Mat clone = workingMat.clone();
        pipeline.process(clone);
        workingMat = pipeline.cvErodeOutput();
        clone.release();
        Rect bestRect = null;
        double bestDifference = 0;

        List<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(workingMat, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
        for(MatOfPoint cont : contours) {
            double score = calculateScore(cont, workingMat); // Get the difference score using the scoring API

            // Get bounding rect of contour
            Rect rect = Imgproc.boundingRect(cont);

            // If the result is better then the previously tracked one, set this rect as the new best
            if (score > bestDifference) {
                bestDifference = score;
                bestRect = rect;
                if(isValidContour(cont)){
                    if(bestContour != null)bestContour.release();
                    bestContour = cont;
                }
                else {
                    bestContour = null;
                    cont.release();
                }
            } else cont.release();
        }
        workingMatIn.copyTo(workingMatOut);
        if(bestRect != null) {
            Imgproc.rectangle(workingMatOut, bestRect.tl(), bestRect.br(), new Scalar(255,0,0),4);
        }

        /*redFilter.process(workingMatIn, workingMat);
        Imgproc.cvtColor(workingMat,workingMat,Imgproc.COLOR_GRAY2RGB);
        Core.bitwise_and(workingMatIn,workingMat,workingMatOut);*/

        hierarchy.release();
        workingMatIn.release();
        workingMat.release();
        pipeline.releaseAll();
        if(bestRect != null)
        System.gc();
        return workingMatOut;
    }

    private double calculateScore(MatOfPoint contour, Mat input) {
        if(Imgproc.contourArea(contour) < 30) {
            return 0;
        }
        double score = 0;
        score -= ((double)input.width() / 2 - (double)Imgproc.boundingRect(contour).x);
        score += Imgproc.contourArea(contour);
        return score;
    }

    private boolean isValidContour(MatOfPoint contour) {
        return Imgproc.contourArea(contour) >= 30;
    }

    public MatOfPoint getBestContour() {
        return bestContour;
    }

    @Override
    public void useDefaults() {
        // Add in your scorers here.
    }

    private int lastRegisteredWidth = 0, lastRegisteredHeight = 0;

    public double getWidth() {
        return lastRegisteredWidth;
    }

    public double getHeight() {
        return lastRegisteredHeight;
    }
}
