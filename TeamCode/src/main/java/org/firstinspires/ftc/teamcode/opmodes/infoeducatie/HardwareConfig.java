package org.firstinspires.ftc.teamcode.opmodes.infoeducatie;

import android.util.Log;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.teamcode.apis.core_lib.HelixLinkService;
import org.firstinspires.ftc.teamcode.apis.localization.HelixLinkIMU;

import java.io.IOException;
import java.net.ConnectException;

public class HardwareConfig {
    DcMotorEx motorLeft = null;
    DcMotorEx motorRight = null;
    Servo collectorServo = null;
    //HelixLinkIMU imu = null;
    HelixLinkService helixLink = null;
    HardwareMap hardwareMap = null;

    public void init(HardwareMap hardwareMap) {
        this.hardwareMap = hardwareMap;
        helixLink = new HelixLinkService();
        try {
            helixLink.connect("192.168.0.1", 23);
            Log.v("HELIXLINK", "Connected");
        }
        catch (IOException e) {
            System.out.println("HelixLinkConnectionError");
        }

        //imu = new HelixLinkIMU(helixLink);
        motorLeft = hardwareMap.get(DcMotorEx.class, "motorLeft");
        motorRight = hardwareMap.get(DcMotorEx.class, "motorRight");
        collectorServo = hardwareMap.get(Servo.class, "collectorServo");
        motorRight.setDirection(DcMotorSimple.Direction.REVERSE);
        motorLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }
}