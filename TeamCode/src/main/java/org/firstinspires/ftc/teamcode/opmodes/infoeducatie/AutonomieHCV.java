package org.firstinspires.ftc.teamcode.opmodes.infoeducatie;

import android.util.Log;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.apis.HelixCV.HelixCvHelper;
import org.opencv.imgproc.Imgproc;

@TeleOp(name = "Autonom HelixCv")
public class AutonomieHCV extends LinearOpMode {
    HelixCvHelper cv = new HelixCvHelper();
    HardwareConfig hw = new HardwareConfig();

    @Override
    public void runOpMode() throws InterruptedException {
        cv.init(hardwareMap);

        waitForStart();

        hw.init(hardwareMap);
        hw.collectorServo.setPosition(1);
        /*Detect the red thingy
        *
        * */
        hw.motorLeft.setPower(0.2);
        hw.motorRight.setPower(0.2);
        while(!Thread.currentThread().isInterrupted() && opModeIsActive() && !(isStopRequested())) {

            boolean runCv = true;
            int collisionCase = 0;
            try{
                String collisionDetection = hw.helixLink.query("GET COLLISION_DETECTION");
                if(collisionDetection.charAt(0) == '0'){
                    System.out.println("Collision detected: SENSOR_CORNER_LEFT");
                    runCv = false;
                    collisionCase = 5;
                }
                if(collisionDetection.charAt(1) == '0'){
                    System.out.println("Collision detected: SENSOR_CORNER_RIGHT");
                    runCv = false;
                    collisionCase = 4;
                }
                if(collisionDetection.charAt(2) == '0'){
                    System.out.println("Collision detected: SENSOR_FRONT_LEFT");
                    runCv = false;
                    collisionCase = 3;
                }
                if(collisionDetection.charAt(3) == '0'){
                    System.out.println("Collision detected: SENSOR_FRONT_RIGHT");
                    runCv = false;
                    collisionCase = 2;

                }
                if(collisionDetection.charAt(4) == '0'){
                    System.out.println("Collision detected: SENSOR_FRONT_CENTER");
                    runCv = false;
                    collisionCase = 1;
                }
                switch(collisionCase) {
                    case 1:
                        //center
                        hw.motorLeft.setPower(-0.2);
                        hw.motorRight.setPower(-0.2);
                        sleep(3000);
                        hw.motorLeft.setPower(0.2);
                        hw.motorRight.setPower(-0.2);
                        break;
                    case 2:
                        //fr
                        hw.motorLeft.setPower(-0.2);
                        hw.motorRight.setPower(-0.2);
                        sleep(2000);
                        hw.motorLeft.setPower(-0.2);
                        hw.motorRight.setPower(0.2);
                        break;
                    case 3:
                        //fl
                        hw.motorLeft.setPower(-0.2);
                        hw.motorRight.setPower(-0.2);
                        sleep(2000);
                        hw.motorLeft.setPower(0.2);
                        hw.motorRight.setPower(-0.2);

                        break;
                    case 4:
                        //cr
                        hw.motorLeft.setPower(-0.2);
                        hw.motorRight.setPower(-0.2);
                        sleep(2000);
                        hw.motorLeft.setPower(-0.2);
                        hw.motorRight.setPower(0.2);
                        break;
                    case 5:
                        //cl
                        hw.motorLeft.setPower(-0.2);
                        hw.motorRight.setPower(-0.2);
                        sleep(2000);
                        hw.motorLeft.setPower(-0.2);
                        hw.motorRight.setPower(0.2);
                        break;
                }

            }
            catch (StringIndexOutOfBoundsException e) {
                hw.helixLink.close();
                break;
            }
            if(!runCv) {
                sleep(800);
                hw.motorLeft.setPower(0.2);
                hw.motorRight.setPower(0.2);
            }
            if(cv.runDetector() != null && runCv) {
                //detected
                Log.v("AUTONOM", "Detectat");
                telemetry.addData("Best contour",cv.bestContour == null);
                if(Math.abs(cv.getDistFromVerticalCenter()) > 50){
                    hw.motorLeft.setPower(-Range.clip(cv.getDistFromVerticalCenter()/880, -0.1, 0.1));
                    hw.motorRight.setPower(Range.clip(cv.getDistFromVerticalCenter()/880, -0.1, 0.1));
                    telemetry.addData("Detector",Range.clip(cv.getDistFromVerticalCenter()/880, -0.1, 0.1));
                }
                else {
                    hw.motorLeft.setPower(0.2);
                    hw.motorRight.setPower(0.2);
                }
                cv.bestContour.release();
                cv.bestContour = null;
            }
            resetStartTime();
            System.gc();
            idle();
            msStuckDetectStart = 900000000;
            msStuckDetectLoop = 900000000;
            telemetry.update();
        }

    }
}
