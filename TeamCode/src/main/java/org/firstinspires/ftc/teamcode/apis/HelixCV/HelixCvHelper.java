package org.firstinspires.ftc.teamcode.apis.HelixCV;

import android.util.Log;

import com.disnodeteam.dogecv.filters.LeviColorFilter;
import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.*;
import org.firstinspires.ftc.teamcode.RobotParams;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

public class HelixCvHelper {
    private Mat workingMat = null;
    public VuforiaFrameSnatch vuforia = null;

    /**Initializes the VuforiaFrameSnatch object and
     * selects capture source.
     *
     * @param hardwareMap
     */


    static {
        System.loadLibrary("opencv_java3");
    }

    public void init(HardwareMap hardwareMap) {
        Parameters params;

        params = new Parameters(hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId","id",hardwareMap.appContext.getPackageName()));
        params.vuforiaLicenseKey = RobotParams.VUFORIA_LICENSE_KEY;
        params.cameraMonitorFeedback = VuforiaLocalizer.Parameters.CameraMonitorFeedback.AXES;
        params.fillCameraMonitorViewParent = false;

        //Uncomment for phone camera
        params.cameraDirection = VuforiaLocalizer.CameraDirection.BACK;

        hierarchy = new Mat();

        //Uncomment for webcam
        //WebcamName webcamName = hardwareMap.get(WebcamName.class, "Webcam 1");
        //params.cameraName = webcamName;

        vuforia = new VuforiaFrameSnatch(params);
    }

    private LeviColorFilter redFilter = new LeviColorFilter(LeviColorFilter.ColorPreset.RED);

    private Mat hierarchy = null;

    public MatOfPoint bestContour = null;

    private RedPipeline pipeline = new RedPipeline();

    /**Returns trackable contour object if detection succeeds.
     * If detection fails returns null.
     * @return OpenCV MatOfPoint Contour
     */

    private double width = 0;
    private double height = 0;

    public MatOfPoint runDetector() {
        Log.v("HELIXCV", "Processing frame...");
        bestContour = null;
        workingMat = vuforia.getCameraMat(-1);
        if(workingMat == null) return null;
        Log.v("HELIXCV", Integer.toString(workingMat.width() * workingMat.height()));
        width = workingMat.width();
        height = workingMat.height();
        Mat clone = workingMat.clone();
        pipeline.process(clone);
        clone.release();
        workingMat = pipeline.cvErodeOutput().clone();
        pipeline.releaseAll();

        Rect bestRect = null;
        double bestDifference = 0;

        List<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(workingMat, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
        for(MatOfPoint cont : contours) {
            double score = calculateScore(cont); // Get the difference score using the scoring API

            // Get bounding rect of contour
            Rect rect = Imgproc.boundingRect(cont);

            // If the result is better then the previously tracked one, set this rect as the new best
            if (score > bestDifference && score != 0) {
                bestDifference = score;
                bestRect = rect;
                if(isValidContour(cont))bestContour = cont;
                else {
                    bestContour = null;
                    if(cont != null)cont.release();
                }
            } else if(cont != null)cont.release();
        }
        cleanup();
        return bestContour;
    }

    private double calculateScore(MatOfPoint contour) {
        if(Imgproc.contourArea(contour) < 10) {
            return 0;
        }
        double score = 0;
        score -= Math.abs((double)workingMat.width() / 2 - (double)Imgproc.boundingRect(contour).x);
        score += Imgproc.contourArea(contour);
        return score;
    }

    private boolean isValidContour(MatOfPoint contour) {
        return Imgproc.contourArea(contour) >= 10;
    }

    /**Releases unused public matrices.
     * Triggers garbage collector.
     */
    public void cleanup() {
        if(workingMat != null) workingMat.release();
        if(hierarchy != null) hierarchy.release();
        //if(bestContour != null) bestContour.release();
        System.gc();
    }

    public double getDistFromVerticalCenter() {
        Rect boundingBox;
        if(bestContour != null){boundingBox = Imgproc.boundingRect(bestContour);
        return  boundingBox.x + (double)boundingBox.width/2 - (width / 2);
        }
        return 0;
    }
}
