package org.firstinspires.ftc.teamcode.apis.motors;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.PIDFCoefficients;

import org.firstinspires.ftc.teamcode.apis.core_lib.Point;

/**Useful for basic autonomous drive
 * providing safe and reliable motor control simultaneously for four of them.
 * Common use cases: mecanum wheels or holonomic omni wheels.
 */
public class DrivetrainQuad {
    public DcMotorEx frontLeftDrive, frontRightDrive;
    public DcMotorEx rearLeftDrive, rearRightDrive;

    /**May be used by classes that extend DrivetrainQuad
     * for providing additional functionality.
     */
    public double frontLeftSpeed = 0, frontRightSpeed = 0, rearLeftSpeed = 0, rearRightSpeed = 0;

    public DrivetrainQuad(DcMotor _frontLeftDrive, DcMotor _frontRightDrive, DcMotor _rearLeftDrive, DcMotor _rearRightDrive) {
        frontLeftDrive = (DcMotorEx) _frontLeftDrive;
        frontRightDrive = (DcMotorEx) _frontRightDrive;
        rearLeftDrive = (DcMotorEx) _rearLeftDrive;
        rearRightDrive = (DcMotorEx) _rearRightDrive;

        frontLeftDrive.setPower(0);
        frontLeftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        frontLeftDrive.setTargetPosition(0);
        frontLeftDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        frontRightDrive.setPower(0);
        frontRightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        frontRightDrive.setTargetPosition(0);
        frontRightDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        rearLeftDrive.setPower(0);
        rearLeftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rearLeftDrive.setTargetPosition(0);
        rearLeftDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        rearRightDrive.setPower(0);
        rearRightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rearRightDrive.setTargetPosition(0);
        rearRightDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        //Making sure motor corrects its position if overshot.
        frontLeftDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        frontRightDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rearLeftDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rearRightDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    }

    /**Sets motor speed.
     * Direction adjusts automatically.
     * @param speed Double ranging from 0 to 1.
     */
    public void setSpeed(double speed) {
        if(speed < 0) speed = 0;
        frontLeftSpeed = speed;
        frontRightSpeed = speed;
        rearLeftSpeed = speed;
        rearRightSpeed = speed;
    }

    /**Moves the motors the specified number of steps(encoder counts).
     * Runs until all of them stop.<br>
     * FUNDAMENTAL, do not override!
     * @param frontLeftSteps Can also be negative.
     * @param frontRightSteps Can also be negative.
     * @param rearLeftSteps Can also be negative.
     * @param rearRightSteps Can also be negative.
     */
    public void move(int frontLeftSteps, int frontRightSteps, int rearLeftSteps, int rearRightSteps) {
        try {
            frontLeftDrive.setTargetPosition(frontLeftSteps);
            frontRightDrive.setTargetPosition(frontRightSteps);
            rearLeftDrive.setTargetPosition(rearLeftSteps);
            rearRightDrive.setTargetPosition(rearRightSteps);

            frontLeftDrive.setPower(frontLeftSpeed);
            frontRightDrive.setPower(frontRightSpeed);
            rearLeftDrive.setPower(rearLeftSpeed);
            rearRightDrive.setPower(rearRightSpeed);

            while(frontLeftDrive.isBusy() || frontRightDrive.isBusy() || frontLeftDrive.isBusy() || frontRightDrive.isBusy()) {
                if(Thread.currentThread().isInterrupted()) throw new InterruptedException();
                if(!frontLeftDrive.isBusy()) {
                    frontLeftDrive.setPower(0);
                    frontLeftDrive.setMotorDisable();
                    frontLeftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    frontLeftDrive.setTargetPosition(0);
                    frontLeftDrive.setMotorEnable();
                }
                if(!frontRightDrive.isBusy()) {
                    frontRightDrive.setPower(0);
                    frontRightDrive.setMotorDisable();
                    frontRightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    frontRightDrive.setTargetPosition(0);
                    frontRightDrive.setTargetPosition(0);
                }
                if(!rearLeftDrive.isBusy()) {
                    rearLeftDrive.setPower(0);
                    rearLeftDrive.setMotorDisable();
                    rearLeftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rearLeftDrive.setTargetPosition(0);
                    rearLeftDrive.setMotorEnable();
                }
                if(!rearRightDrive.isBusy()) {
                    rearRightDrive.setPower(0);
                    rearRightDrive.setMotorDisable();
                    rearRightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rearRightDrive.setTargetPosition(0);
                    rearRightDrive.setTargetPosition(0);
                }
            }

        }
        catch (InterruptedException e) {
            //Handling exception for safety purposes, making sure the robot stops moving.
            frontLeftDrive.setMotorDisable();
            frontRightDrive.setMotorDisable();
            frontLeftDrive.setMotorDisable();
            frontRightDrive.setMotorDisable();
        }
    }

    /**Simulates the move() function of a DualDrivetrain
     * @param leftSteps
     * @param rightSteps
     */
    public void moveLikeDual(int leftSteps, int rightSteps) {
        try {
            frontLeftDrive.setTargetPosition(leftSteps);
            frontRightDrive.setTargetPosition(rightSteps);
            rearLeftDrive.setTargetPosition(leftSteps);
            rearRightDrive.setTargetPosition(rightSteps);

            frontLeftDrive.setPower(frontLeftSpeed);
            frontRightDrive.setPower(frontRightSpeed);
            rearLeftDrive.setPower(rearLeftSpeed);
            rearRightDrive.setPower(rearRightSpeed);

            while(frontLeftDrive.isBusy() || frontRightDrive.isBusy() || frontLeftDrive.isBusy() || frontRightDrive.isBusy()) {
                if(Thread.currentThread().isInterrupted()) throw new InterruptedException();
                if(!frontLeftDrive.isBusy()) {
                    frontLeftDrive.setPower(0);
                    frontLeftDrive.setMotorDisable();
                    frontLeftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    frontLeftDrive.setTargetPosition(0);
                    frontLeftDrive.setMotorEnable();
                }
                if(!frontRightDrive.isBusy()) {
                    frontRightDrive.setPower(0);
                    frontRightDrive.setMotorDisable();
                    frontRightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    frontRightDrive.setTargetPosition(0);
                    frontRightDrive.setTargetPosition(0);
                }
                if(!rearLeftDrive.isBusy()) {
                    rearLeftDrive.setPower(0);
                    rearLeftDrive.setMotorDisable();
                    rearLeftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rearLeftDrive.setTargetPosition(0);
                    rearLeftDrive.setMotorEnable();
                }
                if(!rearRightDrive.isBusy()) {
                    rearRightDrive.setPower(0);
                    rearRightDrive.setMotorDisable();
                    rearRightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rearRightDrive.setTargetPosition(0);
                    rearRightDrive.setTargetPosition(0);
                }
            }

        }
        catch (InterruptedException e) {
            //Handling exception for safety purposes, making sure the robot stops moving.
            frontLeftDrive.setMotorDisable();
            frontRightDrive.setMotorDisable();
            frontLeftDrive.setMotorDisable();
            frontRightDrive.setMotorDisable();
        }
    }


    /**Coefficients for calibrating the feedback of the encoder motors
     * used for eliminating oscillation and stabilizing the output
     * shaft under load. <br>
     * P = proportional coefficient <br>
     * I = integral coefficient <br>
     * D = derivative coefficient. <br>
     * F = feed-forward coefficient. <br>
     * For small, low torque motors with little or no gearing, one procedure you can use to get a good baseline tune is to probe it's response to a disturbance. <br>
     * To tune a PID use the following steps: <br>
     * <ol>
     *  <li>Set all gains to zero.</li>
     *  <li>Increase the P gain until the response to a disturbance is steady oscillation.</li>
     *  <li>Increase the D gain until the the oscillations go away (i.e. it's critically damped).</li>
     *  <li>Repeat steps 2 and 3 until increasing the D gain does not stop the oscillations.</li>
     *  <li>Set P and D to the last stable values.</li>
     *  <li>Increase the I gain until it brings you to the setpoint with the number of oscillations desired (normally zero but a quicker response can be had if you don't mind a couple oscillations of overshoot)</li>
     * </ol>
     * @param frontLeftDrivePID
     * @param frontRightDrivePID
     * @param rearLeftDrivePID
     * @param rearRightDrivePID
     */
    public void setCustomPID(PIDFCoefficients frontLeftDrivePID, PIDFCoefficients frontRightDrivePID, PIDFCoefficients rearLeftDrivePID, PIDFCoefficients rearRightDrivePID) {
        frontLeftDrive.setPIDFCoefficients(DcMotor.RunMode.RUN_TO_POSITION, frontLeftDrivePID);
        frontRightDrive.setPIDFCoefficients(DcMotor.RunMode.RUN_TO_POSITION, frontRightDrivePID);
        rearLeftDrive.setPIDFCoefficients(DcMotor.RunMode.RUN_TO_POSITION, rearLeftDrivePID);
        rearRightDrive.setPIDFCoefficients(DcMotor.RunMode.RUN_TO_POSITION, rearRightDrivePID);
    }

    /**Prevents any power from getting to the motors.<br>
     * Disables braking.
     */
    public void disableMotors() {
        frontLeftDrive.setMotorDisable();
        frontRightDrive.setMotorDisable();
        frontLeftDrive.setMotorDisable();
        frontRightDrive.setMotorDisable();
    }

    public void enableMotors() {
        frontLeftDrive.setMotorEnable();
        frontRightDrive.setMotorEnable();
        frontLeftDrive.setMotorEnable();
        frontRightDrive.setMotorEnable();
    }
}
