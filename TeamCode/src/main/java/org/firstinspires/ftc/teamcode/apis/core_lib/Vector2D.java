package org.firstinspires.ftc.teamcode.apis.core_lib;

public class Vector2D extends Point {

    public Vector2D() {}

    public Vector2D(Vector2D a){
        x = a.x;
        y = a.y;
    }

    public Vector2D(double _x, double _y) {
        x = _x;
        y = _y;
    }

    public Vector2D add(Vector2D b) {
        return new Vector2D(x + b.x, y + b.y);
    }

    public Vector2D subtract(Vector2D b) {
        return new Vector2D(x - b.x, y - b.y);
    }
}
