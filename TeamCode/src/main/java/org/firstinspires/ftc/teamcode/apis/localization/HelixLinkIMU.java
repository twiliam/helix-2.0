package org.firstinspires.ftc.teamcode.apis.localization;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.firstinspires.ftc.teamcode.apis.core_lib.HelixLinkService;


public class HelixLinkIMU extends IMU{
    private HelixLinkService helixLinkInstance = null;
    public HelixLinkIMU(HelixLinkService helixLinkInstance) {
        this.helixLinkInstance = helixLinkInstance;
    }

    private String imuQueryResult = "FAIL";

    private void queryIMUData() {
        if(helixLinkInstance != null && helixLinkInstance.connected) {
            imuQueryResult = helixLinkInstance.query("IMU GET_GYRO");
        }
    }

    public double[] getGyroPose() {
        queryIMUData();
        JsonObject resultObject = null;
        if(!imuQueryResult.equals("FAIL"))resultObject = new JsonParser().parse(imuQueryResult).getAsJsonObject();
        if(resultObject != null)return new double[]{
                resultObject.get("x").getAsDouble(),
                resultObject.get("y").getAsDouble(),
                resultObject.get("z").getAsDouble()
        };
        else return new double[]{};
    }

    public void close() {
        this.helixLinkInstance = null;
        //System.gc();
    }
}
