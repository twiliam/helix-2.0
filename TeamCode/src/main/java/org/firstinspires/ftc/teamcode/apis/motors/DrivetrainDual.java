package org.firstinspires.ftc.teamcode.apis.motors;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.PIDFCoefficients;

import org.firstinspires.ftc.teamcode.apis.localization.IMU;

/**Useful for basic autonomous drive
 * providing safe and reliable motor control simultaneously for two of them.
 */
public class DrivetrainDual {
    public DcMotorEx leftDrive, rightDrive;

    /**May be used by classes that extend DrivetrainDual
     * for providing additional functionality.
     */
    double leftSpeed = 0, rightSpeed = 0;
    double leftSpeedMultiplier = 1, rightSpeedMultiplier = 1;

    public DrivetrainDual(DcMotor _leftDrive, DcMotor _rightDrive) {
        leftDrive = (DcMotorEx) _leftDrive;
        rightDrive = (DcMotorEx) _rightDrive;

        leftDrive.setPower(0);
        leftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftDrive.setTargetPosition(0);
        leftDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        rightDrive.setPower(0);
        rightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightDrive.setTargetPosition(0);
        rightDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        //Making sure motor corrects its position if overshot.
        leftDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    }

    /**Sets motor speed.
     * Direction adjusts automatically.
     * @param speed Double ranging from 0 to 1.
     */
    public void setSpeed(double speed) {
        if(speed < 0) speed = 0;
        leftSpeed = speed;
        rightSpeed = speed;
    }

    /**Moves the motors the specified number of steps(encoder counts).
     * Runs until both of them stop.<br>
     * FUNDAMENTAL, do not override!
     * @param leftSteps Can also be negative.
     * @param rightSteps Can also be negative.
     */
    public void move(int leftSteps, int rightSteps) {
        try {
            leftDrive.setTargetPosition(leftSteps);
            rightDrive.setTargetPosition(rightSteps);

            leftDrive.setPower(leftSpeed * leftSpeedMultiplier);
            rightDrive.setPower(rightSpeed * rightSpeedMultiplier);

            while(leftDrive.isBusy() || rightDrive.isBusy()) {
                if(Thread.currentThread().isInterrupted()) throw new InterruptedException();

                if(!leftDrive.isBusy()) {
                    leftDrive.setPower(0);
                    leftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftDrive.setTargetPosition(0);
                }
                if(!rightDrive.isBusy()) {
                    rightDrive.setPower(0);
                    rightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightDrive.setTargetPosition(0);
                }
                eventLoop(leftSteps, rightSteps);
            }

        }
        catch (InterruptedException e) {
            //Handling exception for safety purposes, making sure the robot stops moving.
            leftDrive.setMotorDisable();
            rightDrive.setMotorDisable();
        }
    }


    /**Coefficients for calibrating the feedback of the encoder motors
     * used for eliminating oscillation and stabilizing the output
     * shaft under load. <br>
     * P = proportional coefficient <br>
     * I = integral coefficient <br>
     * D = derivative coefficient. <br>
     * F = feed-forward coefficient.
     * @param leftDrivePID
     * @param rightDrivePID
     */
    public void setCustomPID(PIDFCoefficients leftDrivePID, PIDFCoefficients rightDrivePID) {
        leftDrive.setPIDFCoefficients(DcMotor.RunMode.RUN_TO_POSITION, leftDrivePID);
        rightDrive.setPIDFCoefficients(DcMotor.RunMode.RUN_TO_POSITION, rightDrivePID);
    }

    /**Prevents any power from getting to the motors.<br>
     * Disables braking.
     */
    public void disableMotors() {
        leftDrive.setMotorDisable();
        rightDrive.setMotorDisable();
    }

    public void enableMotors() {
        leftDrive.setMotorEnable();
        rightDrive.setMotorEnable();
    }

    /**Rotates robot by circumference measured in encoder steps.
     * <b>positive number of steps</b> rotates counterclockwise (left).
     * <b>negative number of steps</b> rotates clockwise (right).
     * @param steps encoder steps (counts)
     */
    public void rotate(int steps) {
        move(-steps, steps);
    }

    /**Moves the robot forwards(+) and backwards(-).
     * @param steps encoder steps (counts)
     */
    public void moveOnY(int steps) {
        move(steps, steps);
    }

    public void eventLoop(int leftSteps, int rightSteps) {
        if(leftSteps == rightSteps) {
            //gyro keep line
            lockToAngle(getCurrentAngle());
            if(lockedToAngle && angleControlEnabled) {
                double correction = targetAngle - getCurrentAngle();
                rightSpeedMultiplier += correction / 80;
                leftSpeedMultiplier -= correction / 80;
            }
            unlockAngle();
        }
        if(leftSteps == -rightSteps) {
            //gyro to angle

        }
    }

    private boolean lockedToAngle = false;
    private double targetAngle = 0;

    private void lockToAngle(double targetAngle) {
        lockedToAngle = true;
        if(angleControlEnabled) {
            this.targetAngle = getCurrentAngle() + targetAngle;
        }
    }

    private void unlockAngle() {
        lockedToAngle = false;
    }

    //Angle control
    IMU angleControlIMU = null;
    private boolean angleControlEnabled = false;

    void setAngleControlIMU(IMU imu) {
        angleControlIMU = imu;
    }

    private double getCurrentAngle() {
        if(angleControlEnabled) {
            return -angleControlIMU.getGyroPose()[0];
        }
        else return 0;
    }

    public void enableAngleControl() {
        angleControlEnabled = true;
    }

    public void disableAngleControl() {
        angleControlEnabled = false;
    }
}
