package org.firstinspires.ftc.teamcode.opmodes.infoeducatie;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcore.external.Telemetry;

@TeleOp(name = "Teleop")
public class RemoteControl extends OpMode {
    HardwareConfig hw = new HardwareConfig();
    @Override
    public void init() {
        hw.init(hardwareMap);
    }


    double powerMultiplier = 1;

    @Override
    public void loop() {

        //telemetry.addData("Senzori distanta", hw.helixLink.query("GET COLLISION_DETECTION"));
        double leftPower, rightPower;
        double drive = - gamepad1.left_stick_y;
        double turn = 0;
        if(drive < 0){
            turn = -gamepad1.right_stick_x;
        }
        else {
            turn = gamepad1.right_stick_x;
        }

        telemetry.addData("Left Stick Y", gamepad1.left_stick_y);
        telemetry.addData("Right Stick X", gamepad1.right_stick_y);
        telemetry.addData("Drive", drive);
        telemetry.addData("Turn", turn);
        telemetry.update();


        leftPower    = Range.clip(drive - turn, -1.0, 1.0) ;
        rightPower   = Range.clip(drive + turn, -1.0, 1.0) ;

        double collectorPower = 0.5;
        collectorPower -= gamepad1.left_trigger / 2;
        collectorPower += gamepad1.right_trigger /2;
        hw.collectorServo.setPosition(collectorPower);

        if(gamepad1.right_bumper) powerMultiplier = 1;
        if(gamepad1.left_bumper) powerMultiplier = 0.5;

        hw.motorLeft.setPower(leftPower * powerMultiplier);
        hw.motorRight.setPower(rightPower * powerMultiplier);
    }
}
