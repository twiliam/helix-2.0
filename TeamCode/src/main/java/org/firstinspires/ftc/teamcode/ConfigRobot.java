package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;

@TeleOp(name = "OPmode de jale")
public class ConfigRobot extends OpMode {
    DcMotor motorr = null;
    DcMotor motorl = null;
    Servo servo1 = null;

    @Override
    public void init() {
        motorr = hardwareMap.get(DcMotor.class, "motorr");
        motorl = hardwareMap.get(DcMotor.class, "motorl");
        servo1 = hardwareMap.get(Servo.class, "s1");
    }

    @Override
    public void loop() {
        motorr.setPower(gamepad1.left_stick_y);
        motorl.setPower(gamepad1.right_stick_y);
        if (gamepad1.dpad_left)servo1.setPosition(0);
        if (gamepad1.dpad_right)servo1.setPosition(1);
    }
}
