package org.firstinspires.ftc.teamcode.apis.motors;

import org.firstinspires.ftc.teamcode.apis.core_lib.Point;

public class MovementController {
    Object drivetrain = null;

    DrivetrainDual drivetrainDual = null;
    DrivetrainQuad drivetrainQuad = null;

    BehaviourDualDrivetrain behaviourDualDrivetrain = null;

    //Dimensions
    public double axleLength = 31.3;
    public double encoderCountsPerCm = 40;

    public MovementController(DrivetrainDual _drivetrain) {
        drivetrain = _drivetrain;
        drivetrainDual = _drivetrain;
    }

    public MovementController(DrivetrainQuad _drivetrain) {
        drivetrain = _drivetrain;
        drivetrainQuad = _drivetrain;
    }

    public boolean hasValidDrivetrain() {
        return (drivetrain instanceof DrivetrainDual) || (drivetrain instanceof DrivetrainQuad);
    }

    /**Handles predictive navigation.<br>
     * <b>BEWARE!!</b> Angles range from -PI(right) to +PI(left),
     * where 0 means center, not actual trigonometric 0.<br>
     * Angles go positive counterclockwise.
     */
    public class BehaviourDualDrivetrain {
        public double x = 0, y = 0, heading = 0;

        final void move(double leftCm, double rightCm){
            if(leftCm ==0 && rightCm == 0) return;

            if(leftCm == - rightCm){
                //rotation
                heading += -(leftCm / (axleLength / 2.0));
                heading = formatAngle(heading);
                System.out.print("heading: ");
                System.out.printf("%.6f",heading);
                System.out.println();

                if(drivetrainDual != null && drivetrain instanceof DrivetrainDual)
                    drivetrainDual.move((int)(leftCm * encoderCountsPerCm), (int)(rightCm * encoderCountsPerCm));
            }
            if(leftCm == rightCm) {
                //Y axis movement
                x += leftCm * Math.cos(heading + Math.PI/2);
                y += leftCm * Math.sin(heading + Math.PI/2);
                if(Math.abs(Math.round(x) - x) < 0.00001)x = Math.round(x);
                if(Math.abs(Math.round(y) - y) < 0.00001)y = Math.round(y);
                System.out.print("x: ");
                System.out.printf("%.6f",x);
                System.out.print(", y: ");
                System.out.printf("%.6f",y);
                System.out.println();
                System.out.println();

                if(drivetrainDual != null)
                    drivetrainDual.move((int)(leftCm * encoderCountsPerCm), (int)(rightCm * encoderCountsPerCm));
            }

            //Adding steps to the simulator
            Point position = new Point(10*x,10*y);
            position.u = Math.toDegrees(-heading);
            //scad.addStep(position);

        }

        /**More robust implementation of run(movement_type = predictive_navigation).
         * @param point
         * @param directionOfSteering
         */
        public void navigateTo(Point point, boolean directionOfSteering) {
            if(directionOfSteering == Direction.FORWARDS) {
                //The front of the robot drives towards desired position.
                double dist = Math.sqrt(Math.pow(point.x - x, 2) + Math.pow(point.y - y, 2));
                angularTurn(formatAngle(-heading + relativeAngle(new Point(point.x - x, point.y - y))));
                move(dist, dist);
            }
            if(directionOfSteering == Direction.BACKWARDS) {
                //The read of the robot drives towards desired position.
                double dist = Math.sqrt(Math.pow(point.x - x, 2) + Math.pow(point.y - y, 2));
                angularTurn(formatAngle(- Math.PI - heading + relativeAngle(new Point(point.x - x, point.y - y))));
                move(-dist, -dist);
            }
        }

        /**Moves the robot according to vector given
         * in reference to initial cartesian system
         * conferring the dual drivetrain the ability
         * to emulate a mecanum drivetrain swerving
         * parallel to its initial axes.
         * Replaces run mode <i>move_parallel_to_initial_axis</i>
         * @param vector
         */
        public void moveAddVectorToPosition(Point vector) {
            double dist = Math.sqrt(Math.pow(vector.x, 2) + Math.pow(vector.y, 2));
            double angle = -heading + relativeAngle(new Point(vector.x, vector.y));
            if(Math.abs(angle) <= Math.PI / 2){
                angularTurn(formatAngle(angle));
                move(dist, dist);
            }
            if(Math.abs(angle) > Math.PI / 2){
                angularTurn(formatAngle(angle + Math.PI));
                move(-dist, -dist);
            }

        }

        /**Moves by a given vector using
         * the robot as the reference system.<br>
         * Steers with the front.
         * @param vector
         */
        public void moveForwardsByRelativeVector(Point vector) {
            double dist = Math.sqrt(Math.pow(vector.x, 2) + Math.pow(vector.y, 2));
            double angle = relativeAngle(new Point(vector.x, vector.y));
            angularTurn(formatAngle(angle));
            move(dist, dist);
        }

        /**Moves by a given vector using
         * the robot as the reference system.<br>
         * Steers with the rear.
         * @param vector
         */
        public void moveBackwardsByRelativeVector(Point vector) {
            double dist = Math.sqrt(Math.pow(vector.x, 2) + Math.pow(vector.y, 2));
            double angle = -relativeAngle(new Point(vector.x, vector.y));
            angularTurn(formatAngle(angle));
            move(-dist, -dist);
        }

        /**Turns towards a given point
         * in the local relative plane
         * of the robot.
         * @param point
         */
        void turnToPoint(Point point) {
            double turn_angle = relativeAngle(new Point(point.x, point.y));
            angularTurn(turn_angle);
        }

        void turnReverseToPoint(Point point) {
            double turn_angle = -relativeAngle(new Point(point.x, point.y));
            angularTurn(turn_angle);
        }

        /**<b>BEWARE!!</b> Angles range from -PI(right) to +PI(left),
         * where 0 means center, not actual trigonometric 0.<br>
         * Angles go positive counterclockwise.
         * @param angle
         */
        void angularTurn(double angle) {
            move(- angle * (axleLength / 2.0), angle * (axleLength / 2.0));
        }

        double relativeAngle(Point point){
            return Math.atan2(point.y, point.x) - Math.PI / 2;
        }

        /**Generates a vector of length 1 of specified angle.<br>
         * <b>BEWARE!!</b> Angles range from -PI(right) to +PI(left),
         * where 0 means center, not actual trigonometric 0.<br>
         * Angles go positive counterclockwise.
         * @param angle
         * @return
         */
        Point pointOfAngle(double angle) {
            return new Point(Math.cos(angle + (Math.PI / 2)) , Math.sin(angle + (Math.PI / 2)));
        }

        /**Formats angle to +PI <-> -PI range.
         * @param angle
         */
        double formatAngle(double angle) {
            return relativeAngle(pointOfAngle(angle));
        }

        public class Direction {
            static final boolean FORWARDS = true;
            static final boolean BACKWARDS = false;
        }

        //OpenSCAD Simulator
        /*public OpenScadAnimation scad = null;

        public void startOpenScadAnimation() {
            scad = new OpenScadAnimation("/home/madnerd/Desktop/robotmovement.scad");
        }

        public void saveOpenScadAnimation() {
            scad.save();
        }*/
    }
}
