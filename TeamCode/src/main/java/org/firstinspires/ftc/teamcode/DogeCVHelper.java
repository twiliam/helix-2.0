package org.firstinspires.ftc.teamcode;

import com.disnodeteam.dogecv.CameraViewDisplay;
import com.disnodeteam.dogecv.DogeCV;
import com.disnodeteam.dogecv.detectors.DogeCVDetector;
import com.disnodeteam.dogecv.detectors.roverrukus.GoldDetector;
import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.robotcore.external.hardware.camera.Camera;
import org.firstinspires.ftc.robotcore.external.hardware.camera.CameraName;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.teamcode.legacy.RobotParams;

public class DogeCVHelper {
    DogeCV.CameraMode cameraMode = null;
    boolean findVuMarks = false;

    CameraName webcam = null;

    public AdvancedDetector detector = null;

    public void init(HardwareMap hardwareMap) {
        detector = new AdvancedDetector();

        detector.VUFORIA_KEY = RobotParams.VUFORIA_LICENSE_KEY;

        //Webcam support
        webcam = hardwareMap.get(WebcamName.class, "Webcam 1");
        cameraMode = DogeCV.CameraMode.WEBCAM;
        detector.init(hardwareMap.appContext, CameraViewDisplay.getInstance(), cameraMode, findVuMarks, webcam);


        //detector.init(hardwareMap.appContext, CameraViewDisplay.getInstance(), cameraMode, findVuMarks);
        detector.useDefaults();

        //Settings
        detector.alignSize = 350; // How wide (in pixels) is the range in which the gold object will be aligned. (Represented by green bars in the preview)
        detector.alignPosOffset = 10; // How far from center frame to offset this alignment zone.
        detector.downscale = 0.5; // How much to downscale the input frames
        detector.areaScoringMethod = DogeCV.AreaScoringMethod.MAX_AREA; // Can also be PERFECT_AREA
        detector.perfectAreaScorer.perfectArea = 5000; // if using PERFECT_AREA scoring
        detector.speed = DogeCV.DetectionSpeed.FAST;
        detector.maxAreaScorer.weight = 0.3;

        detector.ratioScorer.weight = 5; //
        detector.ratioScorer.perfectRatio = 1.0; // Ratio adjustment

        detector.enable();
    }

    public void start() {

    }

    public void stop() {
        if(detector != null) detector.disable();
    }

    public double getXval() {
        return detector.getXPosition();
    }

    public boolean aligned() {
        long current_time = System.currentTimeMillis();
        while(System.currentTimeMillis() - current_time < 350 && !Thread.currentThread().isInterrupted());
        return detector.getAligned();
    }


    public boolean inFrame() {
        return detector.isFound();
    }
}
