package org.firstinspires.ftc.teamcode.apis.HelixCV;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.qualcomm.robotcore.util.RobotLog;
import com.vuforia.Frame;
import com.vuforia.Image;
import com.vuforia.PIXEL_FORMAT;
import com.vuforia.State;
import com.vuforia.Vuforia;

import org.firstinspires.ftc.robotcore.internal.vuforia.VuforiaLocalizerImpl;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Core;
import org.opencv.core.MatOfPoint;
import org.opencv.imgproc.Imgproc;

import java.nio.ByteBuffer;
import java.util.Locale;

public class VuforiaFrameSnatch extends VuforiaLocalizerImpl {
    public Image rgb = null;

    private Mat resultMat = new Mat();
    private ByteBuffer pixelBuffer = null;

    class ClosableFrame extends Frame {
        public ClosableFrame(Frame other){
            super(other);
        }
        public void close(){
            super.delete();
        }
    }

    @Override
    public void close() {
        super.close();
    }

    public VuforiaFrameSnatch(Parameters parameters) {
        super(parameters);
        stopAR();
        clearGlSurface();
        this.vuforiaCallback = new VuforiaFrameSnatch.VuforiaCallbackSubclass();
        startAR();
        Vuforia.setFrameFormat(PIXEL_FORMAT.RGB565,true);
        Vuforia.setFrameFormat(PIXEL_FORMAT.RGB888,false);

    }

    public class VuforiaCallbackSubclass extends VuforiaCallback {
        @Override
        public synchronized void Vuforia_onUpdate(State state){
            int dpf = PIXEL_FORMAT.RGB565;
            super.Vuforia_onUpdate(state);
            VuforiaFrameSnatch.ClosableFrame frame = new VuforiaFrameSnatch.ClosableFrame(state.getFrame());
            long num = frame.getNumImages();
            for(int i = 0; i < num; i++) {
                if(frame.getImage(i).getFormat() == dpf){
                    rgb = frame.getImage(i);
                    RobotLog.v(String.format(Locale.US,"Frame index :%d", frame.getIndex()));
                    RobotLog.v(String.format(Locale.US,"Frame index :%s valid", rgb != null));
                    break;
                }
            }

            frame.close();
        }
    }

    public void clearGlSurface(){
        if(this.glSurfaceParent != null) {
            appUtil.synchronousRunOnUiThread(new Runnable() {
                @Override
                public void run() {
                    glSurfaceParent.removeAllViews();
                    glSurfaceParent.getOverlay().clear();
                    glSurface = null;
                }
            });
        }
    }

    public Mat getCameraMat(int flipCase) {
        Bitmap bm = null;
        bm = Bitmap.createBitmap(rgb.getWidth(),rgb.getHeight(), Config.RGB_565);
        pixelBuffer = rgb.getPixels();
        bm.copyPixelsFromBuffer(pixelBuffer);
        Utils.bitmapToMat(bm, resultMat);

        if(flipCase == 1){
            //rotate left
            Core.transpose(resultMat, resultMat);
            Core.flip(resultMat, resultMat, +1);
        }

        if(flipCase == -1){
            //rotate right
            Core.transpose(resultMat, resultMat);
            Core.flip(resultMat, resultMat, +1);
        }

        if(flipCase == 2) {
            //rotate 180
            Core.flip(resultMat, resultMat, -1);
        }

        return resultMat;
    }

    public Mat getCameraMat() {
        return getCameraMat(0);
    }
}
