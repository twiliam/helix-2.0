package org.firstinspires.ftc.teamcode.apis.motors;

import com.qualcomm.robotcore.hardware.DcMotorController;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.PIDCoefficients;
import com.qualcomm.robotcore.hardware.PIDFCoefficients;
import com.qualcomm.robotcore.hardware.configuration.typecontainers.MotorConfigurationType;

import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.teamcode.apis.core_lib.HelixLinkService;

public class HelixLinkDCMotorEx implements DcMotorEx {
    HelixLinkService helixLinkInstance = null;
    String motorId = null;

    int direction = 1;

    double currentPower = 0;

    RunMode currentRunMode = RunMode.RUN_WITHOUT_ENCODER;

    boolean enabled = true;

    public HelixLinkDCMotorEx(String _motorId, HelixLinkService _helixLinkInstance) {
        motorId = _motorId;
        helixLinkInstance = _helixLinkInstance;
    }

    @Override
    public void setMotorEnable() {
        enabled = true;
    }

    @Override
    public void setMotorDisable() {
        setMode(RunMode.RUN_WITHOUT_ENCODER);
        setPower(0);
        enabled = false;
    }

    @Override
    public boolean isMotorEnabled() {
        return enabled;
    }

    @Override
    public void setVelocity(double angularRate) {

    }

    @Override
    public void setVelocity(double angularRate, AngleUnit unit) {

    }

    @Override
    public double getVelocity() {
        return 0;
    }

    @Override
    public double getVelocity(AngleUnit unit) {
        return 0;
    }

    @Override
    public void setPIDCoefficients(RunMode mode, PIDCoefficients pidCoefficients) {

    }

    @Override
    public void setPIDFCoefficients(RunMode mode, PIDFCoefficients pidfCoefficients) throws UnsupportedOperationException {

    }

    @Override
    public void setVelocityPIDFCoefficients(double p, double i, double d, double f) {

    }

    @Override
    public void setPositionPIDFCoefficients(double p) {

    }

    @Override
    public PIDCoefficients getPIDCoefficients(RunMode mode) {
        return null;
    }

    @Override
    public PIDFCoefficients getPIDFCoefficients(RunMode mode) {
        return null;
    }

    @Override
    public void setTargetPositionTolerance(int tolerance) {

    }

    @Override
    public int getTargetPositionTolerance() {
        return 0;
    }

    @Override
    public MotorConfigurationType getMotorType() {
        return null;
    }

    @Override
    public void setMotorType(MotorConfigurationType motorType) {

    }

    @Override
    public DcMotorController getController() {
        return null;
    }

    @Override
    public int getPortNumber() {
        return 0;
    }

    @Override
    public void setZeroPowerBehavior(ZeroPowerBehavior zeroPowerBehavior) {

    }

    @Override
    public ZeroPowerBehavior getZeroPowerBehavior() {
        return null;
    }

    @Override
    public void setPowerFloat() {

    }

    @Override
    public boolean getPowerFloat() {
        return false;
    }

    @Override
    public void setTargetPosition(int position) {

    }

    @Override
    public int getTargetPosition() {
        return 0;
    }

    @Override
    public boolean isBusy() {
        return false;
    }

    @Override
    public int getCurrentPosition() {
        String result = helixLinkInstance.query("[\"" + motorId + "\",\"GET_CURRENT_POS\"]");
        return Integer.parseInt(result);
    }

    @Override
    public void setMode(RunMode mode) {
        if(mode == RunMode.RUN_TO_POSITION) {

        }

        if(mode == RunMode.STOP_AND_RESET_ENCODER) {
            helixLinkInstance.issueCommand("[\"" + motorId + "\",\"SET_POWER\","+ 0 +"]");
            helixLinkInstance.issueCommand("[\"" + motorId + "\",\"RESET_ENCODER\"]");
        }

        if(mode == RunMode.RUN_USING_ENCODER) {

        }

        if(mode == RunMode.RUN_WITHOUT_ENCODER) {

        }
    }

    @Override
    public RunMode getMode() {
        return currentRunMode;
    }

    @Override
    public void setDirection(Direction _direction) {
        if(_direction == Direction.FORWARD) direction = 1;
        if(_direction == Direction.REVERSE) direction = -1;
    }

    @Override
    public Direction getDirection() {
        if(direction < 0) {
            return Direction.REVERSE;
        }
        else {
            return Direction.FORWARD;
        }
    }

    @Override
    public void setPower(double power) {
        currentPower = power;
        if(currentRunMode == RunMode.RUN_WITHOUT_ENCODER){
            int scalePower = (int)power * 255 * direction;
            helixLinkInstance.issueCommand("[\"" + motorId + "\",\"SET_POWER\","+ scalePower +"]");
        }
        if(currentRunMode == RunMode.RUN_WITHOUT_ENCODER){
            int scalePower = Math.abs((int)power * 255);
            helixLinkInstance.issueCommand("[\"" + motorId + "\",\"SET_PID_POWER_LIMIT\","+ scalePower +"]");
        }
    }

    @Override
    public double getPower() {
        return currentPower;
    }

    @Override
    public Manufacturer getManufacturer() {
        return Manufacturer.Unknown;
    }

    @Override
    public String getDeviceName() {
        return "HelixLinkControl";
    }

    @Override
    public String getConnectionInfo() {
        return null;
    }

    @Override
    public int getVersion() {
        return 1;
    }

    @Override
    public void resetDeviceConfigurationForOpMode() {
        setMode(RunMode.RUN_WITHOUT_ENCODER);
        setPower(0);
    }

    @Override
    public void close() {

    }
}
