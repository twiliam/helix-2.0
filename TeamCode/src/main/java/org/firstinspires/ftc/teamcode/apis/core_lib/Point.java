package org.firstinspires.ftc.teamcode.apis.core_lib;

public class Point {
    public Point(double _x, double _y, double _z){
        x = _x;
        y = _y;
        z = _z;
    }

    public Point(double _x, double _y) {
        x = _x;
        y = _y;
    }

    public Point() {
        x = 0;
        y = 0;
        z = 0;
    }

    public double x = 0, y = 0, z = 0;
    public double u = 0, v = 0, w = 0;
}
