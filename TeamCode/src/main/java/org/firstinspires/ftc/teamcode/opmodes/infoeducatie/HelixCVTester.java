package org.firstinspires.ftc.teamcode.opmodes.infoeducatie;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.apis.HelixCV.HelixCvHelper;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

@TeleOp(name="helixcv test")
public class HelixCVTester extends OpMode {
    HelixCvHelper cv = new HelixCvHelper();

    @Override
    public void init() {
        msStuckDetectInit = 900000000;
        cv.init(hardwareMap);
    }

    public void start() {
        Mat output = cv.vuforia.getCameraMat(-1);
        //Imgproc.cvtColor(output.clone(),output,Imgproc.COLOR_BGR5652BGR);
        Imgproc.cvtColor(output.clone(),output,Imgproc.COLOR_RGB2BGR);
        Imgcodecs.imwrite("/sdcard/helixcv.png", output);
    }

    @Override
    public void loop() {

        cv.cleanup();
        requestOpModeStop();
    }

    public void stop() {
        cv.vuforia.close();
    }
}
