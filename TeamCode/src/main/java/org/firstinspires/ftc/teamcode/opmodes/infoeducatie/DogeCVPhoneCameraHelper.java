package org.firstinspires.ftc.teamcode.opmodes.infoeducatie;

import com.disnodeteam.dogecv.CameraViewDisplay;
import com.disnodeteam.dogecv.DogeCV;
import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.robotcore.external.hardware.camera.CameraName;
import org.opencv.imgproc.Imgproc;

public class DogeCVPhoneCameraHelper {
    DogeCV.CameraMode cameraMode = null;
    boolean findVuMarks = false;

    CameraName webcam = null;

    public GoldAlignDetector detector = null;

    public void init(HardwareMap hardwareMap) {
        detector = new GoldAlignDetector();
        cameraMode = DogeCV.CameraMode.BACK;


        //Uncomment for webcam support
        /*
        webcam = hardwareMap.get(WebcamName.class, "Webcam 1");
        detector.init(hardwareMap.appContext, CameraViewDisplay.getInstance(), cameraMode, findVuMarks, webcam);
        cameraMode = DogeCV.CameraMode.WEBCAM;
        */
        detector.init(hardwareMap.appContext, CameraViewDisplay.getInstance(), cameraMode, findVuMarks);

        detector.useDefaults();

        //Settings
        /*detector.alignSize = 350; // How wide (in pixels) is the range in which the gold object will be aligned. (Represented by green bars in the preview)
        detector.alignPosOffset = 10; // How far from center frame to offset this alignment zone.*/
        //detector.downscale = 0.4; // How much to downscale the input frames
        /*detector.areaScoringMethod = DogeCV.AreaScoringMethod.MAX_AREA; // Can also be PERFECT_AREA
        detector.perfectAreaScorer.perfectArea = 5000; // if using PERFECT_AREA scoring*/
        //detector.speed = DogeCV.DetectionSpeed.SLOW;
        /*detector.maxAreaScorer.weight = 0.3;

        detector.ratioScorer.weight = 5; //
        detector.ratioScorer.perfectRatio = 1.0; // Ratio adjustment*/

        detector.enable();
    }

    public void start() {
        if(detector != null) detector.enable();
    }

    public void stop() {
        if(detector != null) detector.disable();
    }


    public double getDistFromVerticalCenter() {
        return detector.getXPosition() + detector.getBestRectWidth()/2 - (detector.getWidth() / 2);
    }
}
