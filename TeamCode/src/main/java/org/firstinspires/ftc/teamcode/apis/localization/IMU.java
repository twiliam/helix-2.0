package org.firstinspires.ftc.teamcode.apis.localization;

public class IMU {
    public double[] getGyroPose() {
        return new double[]{0, 0, 0};
    }

    public double[] getAcceleration() {
        return new double[]{0, 0, 0};
    }
}
